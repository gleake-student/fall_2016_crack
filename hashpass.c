//Galen Leake
//Fall 2016

#include <stdio.h>
#include <string.h>
//need for using the free command to free memory after using pointers
#include <stdlib.h>
//imports md5.h hashfile from current directory
#include "md5.h"


int main(int argc, char *argv[]){
    //Open argv[1] for reading
    FILE *readFile = fopen(argv[1], "r");
    
    if (!readFile){
        printf("Couldn't open startFile\n");
        exit(1);
    }
    
    //Open argv[2]for writing
    FILE *writeFile = fopen(argv[2], "w");
    
    if (!writeFile){
        printf("Couldn't open startFile\n");
        exit(1);
    }
    
    //Create a buffer for the paswords
    char buffArr[20];
    //Loop through all the paswords (while & fgets)
    while (fgets(buffArr, sizeof buffArr, readFile) != NULL){
        //Call md5, get back the hash
        char *hash = md5(buffArr, strlen(buffArr) -1 );//creates hash of string using the md5 hash reading from the character array of strings
        //by using -1 at the end off the line above it deals with the return at the end of line and gives the correct hash
        //Print the hash to the other file
        fprintf(writeFile, "%s\n", hash); //fprintf(where wirting too, "type of data\n", pointer where data is from);
       
       //  free memory
       free(hash);
        
    }

    //Close both files
    fclose(readFile);
    fclose(writeFile);
}